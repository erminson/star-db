import React from 'react';

import './spinner.css';

const Spinner = () => {
  return (
    <div className="loadingio-spinner-double-ring-eiewmlpie1">
      <div className="ldio-6njqo8dg4l">
        <div></div>
        <div></div>
        <div>
          <div></div>
        </div>
        <div>
          <div></div>
        </div>
      </div>
    </div>
  );
};

export default Spinner;
