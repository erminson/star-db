const compose = (...funcs) => (value) => {
  return funcs.reduceRight((acc, func) => func(acc), value);
};

export default compose;
