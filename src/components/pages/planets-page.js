import React, { Component } from 'react';

import ErrorBoundry from '../error-boundry';
import Row from '../row';
import { PlanetList, PlanetDetails } from '../sw-components';

export default class PlanetsPage extends Component {
  state = {
    selectedId: null,
  };

  onItemSelected = (id) => {
    this.setState({
      selectedId: id,
    });
  };

  render() {
    const { selectedId } = this.state;
    return (
      <ErrorBoundry>
        <Row
          left={<PlanetList onItemSelected={this.onItemSelected} />}
          right={<PlanetDetails itemId={selectedId} />}
        />
      </ErrorBoundry>
    );
  }
}
