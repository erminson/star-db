import PersonsPage from './persons-page';
import StarshipsPage from './starships-page';
import PlanetsPage from './planets-page';

import SecretPage from './secret-page';
import LoginPage from './login-page';

export { PersonsPage, StarshipsPage, PlanetsPage, SecretPage, LoginPage };
